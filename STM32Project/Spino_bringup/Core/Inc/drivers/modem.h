#ifndef MODEM_H
#define MODEM_H

#include "../ax25/ax25.h"

#define SIMU_MODE_FILE 1
#define SIMU_MODE_TCP 2

#define MAX_MODE 4
#define DELAY_MULTIMODE 100


int readData(unsigned char *data);
int writeData(s_ax25_packet ax25Frame, int length);
int  setModemMode(uint8_t mode );

extern int gv_simu_mode;

#endif // MODEM_H
