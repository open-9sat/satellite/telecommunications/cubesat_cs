/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

void Error_Handler(void);

static volatile uint64_t ms_counter = 0;


/* I2C link */
#define I2C_BUFFER_SIZE			255
#define I2C_NONE				0
#define I2C_TX					1
#define I2C_RX					2

static volatile uint8_t CSKB_I2C_Dir = I2C_NONE;

static volatile uint8_t CSKB_I2C_TX_Complete = 0;
static volatile uint8_t CSKB_I2C_RX_Complete = 0;

static volatile uint8_t CSKB_I2C_TX_Size = 0;
static volatile uint8_t CSKB_I2C_RX_Size = 0;

static volatile uint8_t CSKB_I2C_NXT_FrameSize = 0;

static volatile uint8_t CSKB_I2C_RX_BUFFER[I2C_BUFFER_SIZE];

/* VHF Link */
#define VHF_MAX_LEN					255

#define VHF_BUF0_STATUS_MASK		1
#define VHF_BUF1_STATUS_MASK		2

static volatile uint8_t current_vhf_buffer = 0;

static volatile uint8_t vhf_buffer_status = 0x00;

static volatile uint8_t VHF_Data_Ready = 0;

static volatile uint8_t VHF_fifo_data_count = 0;

static volatile uint8_t packet_done = 0;

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
