/*
 * SI4463.h
 *
 *  Created on: 31 août 2022
 *      Author: nats
 */

#ifndef INC_SI4463_H_
#define INC_SI4463_H_

#include <radio_config.h>
#include <stdint.h>

#include "si446x_cmd.h"

#include "stm32l4xx_hal.h"

#define SPI1_TOUT		1000

#define RX_FIFO_THRES	20
#define PACKET_SIZE		240
#define NB_FETCH		PACKET_SIZE/RX_FIFO_THRES

typedef enum {
	IDLE,
	RX_PENDING,
	FETCHING_STATUS,
	FETCHING_SIZE,
	FETCHING_DATA,
	CTS_POLLING,
	STATUS_PENDING,
	STATUS_OK

}  SI44_STATES;

typedef enum {
	FETCH_SIZE,
	FETCH_STATUS,
	FETCH_DATA
}  FETCH_OP;

// Dirty hack for SPI read
#define MAX_SPI_BUFFER_READ		255
static volatile uint8_t zero_buff[MAX_SPI_BUFFER_READ] = { 0 };
static volatile uint8_t vhf_packet[MAX_SPI_BUFFER_READ] = { 0 };

// For faster spi transaction we get an extra byte at the beggining of each fetch
// This accessor directly point toward spi frame start
static volatile uint8_t * vhf_packet_accessor[NB_FETCH];

static volatile uint8_t rx_data[32] = { 0 };
static volatile uint8_t vhf_overflow = 0;

static uint8_t cmdArray[] = {\
		SI446X_PATCH_CMDS, \
		0x07, RF_POWER_UP, \
		0x08, RF_GPIO_PIN_CFG, \
		0x06, RF_GLOBAL_XO_TUNE_2, \
		0x05, RF_GLOBAL_CONFIG_1, \
		0x08, RF_INT_CTL_ENABLE_4, \
		0x08, RF_FRR_CTL_A_MODE_4, \
		0x0D, RF_PREAMBLE_TX_LENGTH_9, \
		0x0A, RF_SYNC_CONFIG_6, \
		0x10, RF_PKT_CRC_CONFIG_12, \
		0x10, RF_PKT_RX_THRESHOLD_12, \
		0x10, RF_PKT_FIELD_3_CRC_CONFIG_12, \
		0x10, RF_PKT_RX_FIELD_1_CRC_CONFIG_12, \
		0x09, RF_PKT_RX_FIELD_4_CRC_CONFIG_5, \
		0x08, RF_PKT_CRC_SEED_31_24_4, \
		0x10, RF_MODEM_MOD_TYPE_12, \
		0x05, RF_MODEM_FREQ_DEV_0_1, \
		0x10, RF_MODEM_TX_RAMP_DELAY_12, \
		0x10, RF_MODEM_BCR_NCO_OFFSET_2_12, \
		0x07, RF_MODEM_AFC_LIMITER_1_3, \
		0x05, RF_MODEM_AGC_CONTROL_1, \
		0x10, RF_MODEM_AGC_WINDOW_SIZE_12, \
		0x0E, RF_MODEM_RAW_CONTROL_10, \
		0x06, RF_MODEM_RAW_SEARCH2_2, \
		0x06, RF_MODEM_SPIKE_DET_2, \
		0x05, RF_MODEM_RSSI_MUTE_1, \
		0x09, RF_MODEM_DSA_CTRL1_5, \
		0x10, RF_MODEM_CHFLT_RX1_CHFLT_COE13_7_0_12, \
		0x10, RF_MODEM_CHFLT_RX1_CHFLT_COE1_7_0_12, \
		0x10, RF_MODEM_CHFLT_RX2_CHFLT_COE7_7_0_12, \
		0x05, RF_PA_TC_1, \
		0x0B, RF_SYNTH_PFDCP_CPFF_7, \
		0x10, RF_MATCH_VALUE_1_12, \
		0x0C, RF_FREQ_CONTROL_INTE_8, \
		0x00 \
	};


#endif /* INC_SI4463_H_ */
