#ifndef SP_UART_H
#define SP_UART_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"


void MX_USART1_UART_Init(void);
void writeUART(unsigned char *message, int size );
int readUART(unsigned char *data, int size);

#endif // SP_UART_H
