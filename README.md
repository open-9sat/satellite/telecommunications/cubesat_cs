SPINO Cubesat Communication System
=====


**SPINO is a versatile telecommunication solution suitable for nanosatellites and Cubesats.**



We are a distributed team of contributors, mostly members of the AMSAT-F and the Electrolab hackerspace, but not only. Makers, developers, industrials, researchers teams collaborated on this project.

The Electrolab hackerspace is located in Nanterre, near Paris, in France.


If you're new there, please read the explanations below. _Your contributions are welcome!_



# Introduction
SPINO is a versatile telecommunication solution suitable for nanosatellites and Cubesats.
Operating in UHF and VHF bands, it features tight integration with amateur radio service and the worldwide amateur radio community.
The development of the SPINO SC board was initiated by enthusiats involved in non-profit / educational space projects. Since 2019, the project is supported by the joint efforts of two non-profit organizations: AMSAT-Francophone (site.amsat-f.org) and the hackerspace Electrolab (electrolab.fr).
Thanks to the support provided to "UVSQ-SAT" mission by the amateur radio community (data collection, ground segment support, spectrum coordination support...), LATMOS offered to integrate this experimental board into its new satellite "UVSQ-SAT+" as an additional payload.
In this context, SPINO SC receives since 2021 the academic support of the LATMOS laboratory, and the industrial support of the Adrelys company.
The SPINO SC board features functions dedicated to the spacecraft infrastructure :
* Receiver function for remote control commands from ground…
* Managed or Autonomous beacon (support for OBC failure)
* Data stream (uplink and downlink)
* Antenna deploy support
And functions dedicated to the amateur radio community :
* a versatile digital transponder
* a digital mailbox service

The SPINO SC board will be a pre-validated open source brick available off the shelf for any nanosatellites mission.

By the way, the goal is to maximize compatibility (standardized interfaces with UART, I2C, SPI, CAN FD, and standardizerd PC-104 "like" form factor), and maximize reliability (wide supply voltage range, fail-safe on key points, low power consumption, especially in idle to face failure situations).
SPINO SC operates in the Amateur Radio service bands, and features two full transceivers :
- VHF : TX (+30dBm) and RX 144-146MHz
- UHF : TX (+27dBm) and RX 430-440MHz

# SPINO project license
Documents, parts drawings, manufacturing files and software are gathered on Electrolab's GIT repository:

https://code.electrolab.fr/SPINO/cubesat_cs
Hardware and documents


The SPINO project led by the ELECTROLAB non profit organization, and all documents produced through the development process are under Creative Commons license: 
CC BY-SA 4.0

Attribution-ShareAlike 4.0 International 
https://creativecommons.org/licenses/by-sa/4.0/

Software

The software developed for the needs of the SPINO project led by the ELECTROLAB non profit organization is under the GNU GPL version 3 license.

https://www.gnu.org/licenses/gpl-3.0.txt

