# SpinoSimulateur

SpinoSimulateur allows simulating the operation of the embedded software of the Spino board. The same functional code is used for both the simulator and the embedded software. Only the modem driver code differs. Communication in this case is facilitated via a socket.

The code has been compiled to run on Windows. 

## Usage

1. **Download the SpinoSimulator executable**.
2. **Open a Windows Command Prompt**.
3. **Navigate to the directory containing `spinoSimulator.exe`**.
4. **Execute the simulator**: `spinoSimulator.exe`.
5. **Wait for a connection** to the socket.

## Notes

- Ensure that proper configurations are set up for the socket communication.
- Any modifications to the modem driver code should be made with consideration for compatibility with the simulator and the embedded software.
- For further assistance, refer to the documentation or contact the development team.


