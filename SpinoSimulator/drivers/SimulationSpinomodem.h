#ifndef MODEM_H
#define MODEM_H

#include "../ax25/ax25.h"

#define SIMU_MODE_FILE 1
#define SIMU_MODE_TCP 2

extern int readData(char *data);
extern int writeData(s_ax25_packet ax25Frame, int length);

extern int gv_simu_mode;

#endif // MODEM_H
