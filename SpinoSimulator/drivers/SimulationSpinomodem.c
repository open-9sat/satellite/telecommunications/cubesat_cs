/**
 * \file modem.c
 * \brief modem simulation 
 * \author Xtophe
 * \version 0.2
 * \date 01/08/2022
 *
 *  \todo   To adapt with embeded target
 */

#include <stdio.h>
#include "../ax25/ax25.h"
#include "../drivers/modem.h"
#include "../simulation/lefic.h"
#include "../simulation/SpinoSimuServerTCP.h"

int gv_simu_mode = SIMU_MODE_TCP;

void open() {
	if (gv_simu_mode == SIMU_MODE_FILE) {
		openfile("./header.bin");
	} else if (gv_simu_mode == SIMU_MODE_TCP) {
		TCP_OpenAndWait();
	}
}
//here is the radio TX buffer where to write when the frame is cooked.
//Declaration is performed in the ADF7030.c source file.

int setModemMode(uint8_t mode) {
	int rep = 0;

	return rep;
}

int readData(unsigned char *data) {
	int taille = 0;

	if (gv_simu_mode == SIMU_MODE_FILE) {
		taille = lectureData(data);

	} else if (gv_simu_mode == SIMU_MODE_TCP) {
		PerformSelect(gv_AcceptSocket, gv_m_client_list, 1);
		if (gv_simu_nb_data_received != 0) {
			printf("data received %d", gv_simu_nb_data_received);
			taille = gv_simu_nb_data_received;

			memcpy(data, gv_simu_receiveddata,
					(size_t) gv_simu_nb_data_received);
			data = (char*) gv_simu_receiveddata;
			gv_simu_nb_data_received = 0;
		}
	}

	return taille;

}

int writeData(const s_ax25_packet ax25Frame, const int length) {

	int size = length + (int) sizeof(s_ax25_header);

	printf("Taille message %d -  %d \r\n",length,size);

	if (gv_simu_mode == SIMU_MODE_FILE) {

	} else if (gv_simu_mode == SIMU_MODE_TCP) {
		TCP_Send((char*) &ax25Frame,  (int) size);
		writefiledata(ax25Frame, size);
	}
	return 0;

}
