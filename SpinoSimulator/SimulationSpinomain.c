/**
 * \file main.c
 * \brief launch simulator
 * \author Xtophe
 * \version 0.2
 * \date 01/08/2022
 *
 */

#include <stdio.h>
#include <string.h>
#include "./core/setup.h"

#define STATE_SURVEY 1
#define STATE_MAILBOX 2 

extern void control();
extern void open();

/**
 * \fn int main (void)
 * \brief Entrée du programme.
 *
 * \return 0 - Arrêt normal du programme.
 */

int main(void) {
	printf("Spino Emulation V0.4\n");

	setupGlobalVariable();
inittlm();
	open();
	while (1)
	{
	control();
	}
	printf("END  \n\r");

	return 0;
}
