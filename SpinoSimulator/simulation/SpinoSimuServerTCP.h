#ifndef SPINOSIMUSERVER_H
#define SPINOSIMUSERVER_H

#include <WinSock2.h>

#define MAX_DATA  512
#define MAX_CLIENTS 2

extern char gv_simu_receiveddata[];
extern int gv_simu_nb_data_received;
extern SOCKET gv_m_client_list[MAX_CLIENTS];
extern SOCKET gv_AcceptSocket;

extern void PerformSelect(SOCKET listeningSock, SOCKET clients[],
		int iNumClients);
extern void TCP_OpenAndWait();
extern void TCP_Send(char *data, int nb);

#endif //SPINOSIMUSERVER_H
