#ifndef LEFIC_H
#define LEFIC_H

#include "../ax25/ax25.h"

extern int lectureFile(char *filename, char *data);
extern void openfile(char *filename);
extern int lectureData(char *data);
extern void writefiledata(s_ax25_packet data, int taille);

#endif // LEFIC_H
