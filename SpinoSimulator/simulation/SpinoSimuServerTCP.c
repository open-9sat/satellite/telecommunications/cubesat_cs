/***********************************************************************************************
 *
 *  File Name : tcpServer
 *
 *  Description :  manage tcp server for testing purpose
 *
 *  Usage :  for Spino simulator only
 *
 *  nota : code based on : http://dwise1.net/pgm/sockets/blocking.html
 *
 * *********************************************************************************************
 *
 *  Autor :  Xtophe
 *
 *  date : 2022 / 08 /01
 *
 *  Version :  0.1
 *
 *  todo :
 *
 * **********************************************************************************************
 *
 *  You need to link the library: libws2_32.a
 * OR
 *  -lwsock32
 *  -lws2_32
 *  from c:\TDM-GCC-64\x86_64-w64-mingw32\lib32\
  *  OR
 * c:\MinGW\lib\
  * if you using GCC on windows
 ***********************************************************************************************/

/*=========  INCLUDES ==========================================================================*/

#include <stdio.h>
#include <WinSock2.h>
#include <conio.h>
#include <time.h> 
#include "SpinoSimuServerTCP.h"

/*=========  GLOBAL VARIABLES  ===================================================================*/

char gv_simu_receiveddata[MAX_DATA];
int gv_simu_nb_data_received = 0;

SOCKET m_ServerSock; /* server's listening socket */
SOCKET gv_m_client_list[MAX_CLIENTS];
int m_iNumclients;
SOCKET gv_AcceptSocket;
SOCKET TCPServerSocket;

unsigned char test[6] = { 0x0, 0x01, 0x0F, 0x10, 0xF0, 0xFF };

/*=========  FUNCTIONS  ========================================================================*/

/*---------------------------------------------------------------------------------------------------------
 * 
 *  HandleClient(SOCKET AcceptSocket) :
 * 
 *  description : perform treatment when data are received  
 * 
 * 
 ---------------------------------------------------------------------------------------------------------*/

void HandleClient(SOCKET AcceptSocket) {
// STEP-7 Send Message to Client
	gv_simu_nb_data_received = 0;
	gv_simu_nb_data_received = recv(AcceptSocket, gv_simu_receiveddata,
			MAX_DATA, 0);
	if (gv_simu_nb_data_received == (int) SOCKET_ERROR) {
		printf("SERVER: Receive Failed, Error: %d\n", WSAGetLastError());
		exit(-1);
		gv_simu_nb_data_received = 0;
	}

}

/*---------------------------------------------------------------------------------------------------------
 * 
 *  PerformSelect(SOCKET listeningSock, SOCKET clients[], int iNumClients)) :
 * 
 *  description : see http://dwise1.net/pgm/sockets/blocking.html#SELECT
 * 
 * 
 ---------------------------------------------------------------------------------------------------------*/

void PerformSelect(SOCKET listeningSock, SOCKET clients[], int iNumClients) {
	fd_set sockSet; /* Set of socket descriptors for select() */
	struct timeval selTimeout; /* Timeout for select() */
	int i;
	int iResult;

	/* Zero socket descriptor vector and set for server sockets */
	/* This must be reset every time select() is called */
	FD_ZERO(&sockSet);
	FD_SET(listeningSock, &sockSet);
	for (i = 0; i < iNumClients; i++)
		FD_SET(clients[i], &sockSet);

	/* Timeout specification */
	/* This must be reset every time select() is called */
	selTimeout.tv_sec = 0; /* timeout (secs.) */
	selTimeout.tv_usec = 100000; /* 0 microseconds */

	iResult = select(0, &sockSet, NULL, NULL, &selTimeout);

	if (iResult == -1) {
		/* an error occurred; process it (eg, display error message) */
	} else if (iResult > 0) /* ie, if a socket is ready */
	{
		// test this specific socket to see if it was one of the ones that was set
		//       if (FD_ISSET(listeningSock, &sockSet))
		//       {
		//           AcceptNewClient(listeningSock);
		//       }

		/* Now test the client sockets */
		for (i = 0; i < iNumClients; i++)
			if (FD_ISSET(clients[i], &sockSet)) {
				/* do whatever it takes to read the socket and handle the client */
				/* Please note that this will involve reassociating the socket   */
				/*     with the client record                                    */
				HandleClient(clients[i]);
			}
	}

	/* else iResult == 0, no socket is ready to be read, */
	/*    so ignore them and move on.                    */

}

/*---------------------------------------------------------------------------------------------------------
 * 
 *  OpenAndWait ( ) :
 * 
 *  description :  initialise and wait a client
 * 
 * 
 ---------------------------------------------------------------------------------------------------------*/

void TCP_OpenAndWait() {
	printf("TCP SERVER\n");

	WSADATA Winsockdata;

	struct sockaddr_in TCPServerAddr;
	struct sockaddr_in TCPClientAddr;
	int TCPClientAddrSize = sizeof(TCPClientAddr);

	// STEP-1 WSAStartUp
	if (WSAStartup(MAKEWORD(2, 2), &Winsockdata) != 0) {
		printf("SERVER: WSAStartUp Failed");
	}
	printf("SERVER: WSAStartUp Success\n");

	// STEP-2 Fill TCPServerAddr Struct
	TCPServerAddr.sin_family = AF_INET;
	TCPServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	TCPServerAddr.sin_port = htons(8888);

	//STEP-3 Create Socket
	if ((TCPServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))
			== INVALID_SOCKET) {
		printf("SERVER: TCP Server: Create Socket Failed, Error: %d\n",
				WSAGetLastError());
	}
	printf("SERVER: TCP Server: Create Socket Success\n");

	//STEP-4 bind
	if (bind(TCPServerSocket, (SOCKADDR*) &TCPServerAddr,
			sizeof(TCPServerAddr)) == SOCKET_ERROR) {
		printf("SERVER: Binding Failed, Error: %d", WSAGetLastError());
	}
	printf("SERVER: Binding Success\n");

	//STEP-5 Listen
	if (listen(TCPServerSocket, 2) == SOCKET_ERROR) {
		printf("SERVER: Listen Failed, Error: %d", WSAGetLastError());
	}
	printf("SERVER: Listen Success: Listening for incoming connection...\n");

	// STEP-6 Accept
	if ((gv_AcceptSocket = accept(TCPServerSocket, (SOCKADDR*) &TCPClientAddr,
			&TCPClientAddrSize)) == INVALID_SOCKET) {
		printf("SERVER: Accept Failed, Error: %d\n", WSAGetLastError());
	}
	printf("SERVER: Connection Accepted\n");
	m_ServerSock = 2;
	gv_m_client_list[0] = gv_AcceptSocket;

}

/*---------------------------------------------------------------------------------------------------------
 * 
 *  TCP_closeALL ( ) :
 * 
 *  description :    close all open port.
 * 
 * 
 ---------------------------------------------------------------------------------------------------------*/
void TCP_closeAll() {
	// STEP-9 Close Socket
	if (closesocket(TCPServerSocket) == SOCKET_ERROR) {
		printf("SERVER: Close Socket Failed, Error: %d\n", WSAGetLastError());
	}
	printf("SERVER: Close Socket Success\n");

	// STEP-10 Clean
	if (WSACleanup() == SOCKET_ERROR) {
		printf("SERVER: WSACleanup Failed, Error: %d\n", WSAGetLastError());
	}
	printf("SERVER: WSACleanup Success\n");
}

/*---------------------------------------------------------------------------------------------------------
 * 
 *  TCP_send ( byte data, int nbdata ) :
 * 
 *  description :    send data 
 * 
 * 
 ---------------------------------------------------------------------------------------------------------*/

void TCP_Send(char *data, int nb) {

	 
	if (send(gv_AcceptSocket, data, nb, 0) == SOCKET_ERROR) {
		printf("SERVER: Send Failed Error: %d\n", WSAGetLastError());
	}
	printf("SERVER: Message Sent!\n");
}

// int main() {

//   unsigned char test[6]={0x0,0x01,0x0F,0x10,0xF0,0xFF};
//   TCP_OpenAndWait ();
//   int i;
//   while (1)
//   {
//               PerformSelect(gv_AcceptSocket, gv_m_client_list, 1);

//               if (gv_simu_nb_data_received!=0)
//               {
//                   printf("data recieved %d \r\n",gv_simu_nb_data_received );
//                   gv_simu_nb_data_received=0;
//                   TCP_Send (test,6);
//
//                } else
//                {
//                    printf("wait", i++);
//                }
//
//    }

//}

/*
 run:
 
 TCP SERVER
 SERVER: WSAStartUp Success
 SERVER: TCP Server: Create Socket Success
 SERVER: Binding Success
 SERVER: Listen Success: Listening for incoming connection...
 SERVER: Connection Accepted
 SERVER: Message Sent!
 SERVER: Received Message From Client: Hello from Client!
 SERVER: Close Socket Success
 SERVER: WSACleanup Success
 
 */
