#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ax25/ax25.h"

#define TAILLE_BUF 300   /* valeur quelconque (en gÃ©nÃ©ral, beaucoup plus grande) */

FILE *fichier;

void openfile(char *filename) {

	fichier = fopen(filename, "rb");
	if (fichier == NULL) {
		printf("Ouverture du fichier impossible !");
		exit(0);
	}

}

int lectureData(char *data) {
	int nb_val_lues;

	unsigned char taille;
	// lecture taille data
	nb_val_lues = (int) fread(&taille, sizeof(char), 1, fichier);
	if (nb_val_lues != 0)
		nb_val_lues = (int) fread(data, sizeof(char), (size_t) taille, fichier);
	return nb_val_lues;

}

int lectureFile(char *filename, char *data) {
	FILE *fic;

	//  char [TAILLE_BUF]; /* ce tableau mÃ©morisera les valeurs lues dans le fichier */
	short int nb_val_lues = TAILLE_BUF;
	/* Ouverture du fichier (en lecture binaire) : */
	fic = fopen(filename, "rb");
	if (fic == NULL) {
		printf("Ouverture du fichier impossible !");
		exit(0);
	}
	/* Lecture dans le fichier : */
	printf("\n Liste des valeurs lues : \n");
	/*Remplissage du buffer et traitement, autant de fois que nÃ©cessaire jusqu'Ã  la fin fichier : */
	while (nb_val_lues == TAILLE_BUF) /* vrai tant que fin du fichier non atteinte */
	{

		nb_val_lues = (short int) fread(data, sizeof(char), TAILLE_BUF, fic);
		/* Traitement des valeurs stockÃ©es dans le buffer (ici, un simple affichage) : */
		//    for (i=0; i<nb_val_lues; i++) printf( "%hd", buffer[i] );
	}
	/* Fermeture du fichier : */
	fclose(fic);
	return nb_val_lues;
}

unsigned int nb = 0;

void writefiledata(s_ax25_packet data, int taille) {

	FILE *fics;
	char filename[20] = "result.bin\0";
	char indexFile[20];
	sprintf(indexFile, "%d", nb);
	strcat(indexFile, filename);

	fics = fopen(indexFile, "wb");
	if (fics == NULL) {
		printf("Ouverture du fichier impossible ! %s", indexFile);
		exit(0);
	}
	fwrite(&data.header, sizeof(char), sizeof(s_ax25_header), fics);
	fwrite(data.data, sizeof(char), (size_t) taille, fics);
	fclose(fics);
	nb++;

}
