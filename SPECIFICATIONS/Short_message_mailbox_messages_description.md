| Last modification  	| Status  	|
|--------------------	|---------	|
| 22/02/2022         	| Draft   	|

# Short message mailbox messages description



## Command message 
### AX25 UI Frame 
| Flag | Destination Calssign | Destination SSID  | Source Address | Source SSID | Control Bits | Protocol | Mailbox commandes | Mailbox Parameters | Frame Check Sequence  | Flag |
|------|----------------------|-------------------|----------------|--------------|-------------|----------|-------------------|-------------------|-----------------------------|------|
| 8    | 48                   | 8                 |     48         | 8            | 8           | 8        | 8                 | 0-242             |  16                | 8    |

#### Commands



|            Command Name       | Commande value |                        parameters                        | comment |
|:-----------------------------:|:--------------:|:--------------------------------------------------------:|:-------:|
| Add Message                   |        0       | Message contents [0-242] bytes                           |         |
| Delette message               |        1       | Mailbox Name : (Callsign) 6 byte                         |         |
|                               |                |  Message ID : 1 byte                                     |         |
| Delette mailbox               |        2       | Mailbox Name : (Callsign) 6 byte                         |         |
| List all mailbox              |        3       |                                                          |         |
| List all message in a mailbox |        4       | Mailbox Name : (Callsign) 6 byte                         |         |
| Flush all mailbox             |        5       | secure word :  4 bytes                                   |         |
|                               |                |                                                          |         |


