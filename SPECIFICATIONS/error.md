| Last modification  	| Status  	|
|--------------------	|---------	|
| 05/03/2024         	| Draft   	|

# ERROR DEFINITION 

## 

 ERROR code is return in the response structure after a command sent to SPINO. 

## Table 
|  Error name  | value   | comment  |
| ------------ | ------------ | ------------ |
| SUCCESS  | 0  |  No Error  |
|***Command analysis***| | |
|ERROR_COMMAND_UNKNOW|1 | |
|ERROR_COMMAND_NOT_IMPLEMENTED|2 | |
|ERROR_COMMAND_WITH_WRONG_KEY|3 | |
|ERROR_WRONG_STATE|4 | |
|ERROR_VALUE_FIELD_UNKNOW| 101| |
|***AX25 Error***| | |
|ERROR_AX25_EXCEED_MAX_LENGH |100| |
|ERROR_WRONG_SIZE_RCV |-12;| |
|ERROR_CRC  |-64| |
|***PROG ERROR***|| |
|ERROR_PROG_INDEX_NOT_EQUAL| 128| |
|ERROR_PROG_MEM1_MEM2_NOT_EQUAL |129| |
|ERROR_PROG_INDEX_OUT_OF_BOUND |130| |
| ***MAILBOX ERROR***| | |
|ERROR_MAILBOX_FULL| 10| |
|ERROR_MAILBOX_NOT_FOUND| 11| |
|ERROR_MAILBOX_EMPTY |12| |
|ERROR_ADD_MSG_EXCED_SIZE| 13| |
|ERROR_GET_LAST_MSG_CALLSIGN_WRONG_SIZE| 14| |
|ERROR_MESSAGE_EMPTY |15| |
|ERROR_INFO_MSG_INDEX_OUT_OF_BOUND| 135| |
|ERROR_INFO_MSG_ALREADY_NOT_USED| 136| |
|***EXPERIENCE ERROR***|||
|ERROR_TLE_WRONG_SIZE |140| |
|ERROR_BUFFER_OVERFLOW| -10| |
|ERROR_WRONG_MODEM_MODE |-11| |
|***PAYLOAD ERROR***|||
|ERROR_LOAD_DATA_PAYLOAD_WRONG_SIZE |30| |
