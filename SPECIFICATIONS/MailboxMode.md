| Last modification  	| Status  	|
|--------------------	|---------	|
| 02/03/2022         	| Draft   	|

# Short message mailbox mode 

## Scope

Allows to the users to create a temporary dedicated mailbox to a user and allow him to store short messages for other users 
 

## Maibox Capability 

- up to 64 mailbox available in the same time  (64 users)
- up to 16 messages per mailboxes 
- Message size limited to 242 bytes (message limited to 242 bytes (dest_call_7_bytes + ref_count_7_bytes + 242 = 256) 
- Automatic setup of a new mailbox when an unknown destination station is assessed
- Automatic expiration of messages after 72h
- Automatic expiration of users as soon as the mailbox is empty
- Messages are managed like a FIFO stack

## Commands




### Sending a message 

Scenario to send a message (AX25 header ignored, the considered protocol layer is encapsulated in the payload):
The "ARSxyz" station sends a message to the mailbox
- -> sending "ARSxyz" station identified by its call in the AX25 header
- -> AX25 payload consisting of : Command add message +   message  
- <- SPINO send an acknowledgment message Success or  NACK : limit of the destination mailbox exceeded, or impossible to create a new mailbox


- Command :  CMD_MAILBOX_ADD_MSG
- parameters : message to store  

### request last message

- Command :  CMD_MAILBOX_GET_LAST_MSG :
- parameters :  none


  

### request all mailbox message 


- Command :   CMD_MAILBOX_GET_ALL_MSG
- parameters :   none



### viewing the list of messages of a box 

User send request *List all message in a mailbox* to Satellite with Mailbox Callsign 
If the mailbox exist, the satellite send following information :
- number of message in the mailbox 
- list of message information :  Message id & time   
If the mailbox does not exist, the satellite send a acknolegment with the error message : Mailbox not available  

- Command :   
- parameters :   




### read a message 

User send request *Read message* to Satellite with Mailbox Callsign and Message ID 
If the message in the mailbox exist, the satellite send following information :
- time, 
- message id, 
- message information   

If the message in the mailbox does not exist, the satellite send a acknolegment with the error message : Message  not available  
If the mailbox does not exist, the satellite send a acknolegment with the error message : Mailbox not available  

- Command : CMD_MAILBOX_GET_MSG
- parameters :  message index 
- parameters :  Mailbox Callsign 
 


### delete the first message

User send request *Delette message* to Satellite. 
If the message in the mailbox exist, the satellite send a acknolegment with the information : message deleted
If the message in the mailbox does not exist, the satellite send a acknolegment with the error message : Message  not available  
If the mailbox does not exist, the satellite send a acknolegment with the error message : Mailbox not available  

- Command :   CMD_MAILBOX_DEL_MSG
- parameters :  none 



### viewing the list of active mailboxes

User send request *List all mailbox* to Satellite 
The satellite send following information :

- list of mailbox callsign   
- number of message available  

- Command :   CMD_MAILBOX_GET_LIST_BOX
- parameters :   none



### delete a mailbox 

User send request *Delete mailbox* to Satellite with Mailbox Callsign
If the message in the mailbox exist, the satellite send a acknolegment with the information : mailbox  deleted
If the mailbox does not exist, the satellite send a acknolegment with the error message : Mailbox not available  

- Command :   CMD_MAILBOX_DELETTE_BOX
- parameters :   none


###  flush of mailboxes (Service Message)

User send request *Flush all mailbox* to Satellite 
the satellite delette all mailbox and message
the satellite send a acknolegment with the information : mailboxes deleted

 

- Command :  CMD_MAILBOX_INIT
- parameters : none 


### Dump all Mailbox 

- Command :   CMD_MAILBOX_DUMP_MAILBOX
- parameters :   none



