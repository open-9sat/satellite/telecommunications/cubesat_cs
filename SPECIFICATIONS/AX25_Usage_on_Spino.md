| Last modification  	| Status  	|
|--------------------	|---------	|
| 05/03/2024         	| Draft   	|

# AX25 Usage on Spino
## References 
### Applicable document 
(AX.25 Protocol Specification) [http://www.tapr.org/pdf/AX25.2.2.pdf]
## AX25 : unnumbered frame 
### Description 
As described in the protocol, the unnumbered frame (U frame) is structured as follows :

| Flag     | Address | Control | Info | FCS | Flag     |
|----------|---------|---------|------|-----|----------|
| 01111110 | 112     | 16      | N*8  | 16  | 01111110 |

In the spino use, the frame is used as :  

| Flag | Destination Address  | Source Address | Control Bits | Protocol | Information Field | Frame Check Sequence  | Flag |
|------|----------------------|----------------|--------------|----------|-------------------|-----------------------|------|
| 8    | 56                   | 56             | 8            | 8        | 0-2048            | 16                    | 8    |


#### Flag

The flag field is one octet long. Because the flag delimits frames, it occurs at both the beginning and end of each frame. Two frames may share one flag, which would denote the end of the first frame and the start of the next frame. 
A flag consists of a zero followed by six  ones followed by another zero, or 01111110 (0x7E). 

In order to ensure that the flag bit  sequence mentioned above does not appear accidentally anywhere else in a frame, bit stuffing is applied. The sender monitors the bit sequence for a group of five or more contiguous '1' bits. Any time five contiguous '1' bits are sent, the sending station inserts a '0' bit after the fifth '1' bit. During frame reception, any time five contiguous '1' bits are received, a '0' bit immediately following five '1' bits is discarded.

### Source - Destination address 
Adress consists of the callsign and the Secondary Station Identifier (SSID).
The callsign is made up of 6 upper-case letters, numbers or space ASCII  characters only (7 bits).
The SSID is a four-bit integer that uniquely identifies multiple  stations using the same amateur callsign

The 6 characters of the callsign are placed in the first 6 octets of the field (C1 to C6). Each character bits are shifted one bit on the left and the least significant bit is set to '0'. The SSID is placed in the bits 3-6. The other bits of the field have a fixed value
#### Control Bits (8 bits)
The control field identifies the type of frame being passed and controls several attributes of the Layer 2 connection. For an AX.25 Unnumbered Information Frame, its value is always  00000011 (0x03).
#### Protocol Identifier (8 bits)
Shall be 11110000 (0xF0)

### Frame-Check Sequence (16 bits)
The Frame-Check Sequence is a 16-bit number calculated by both the sender and the receiver  of a frame. It ensures that the frame was not corrupted by the transmission medium. The Frame-Check Sequence is a CRC calculated using polynomial x16 + x12 + x5 + 1 (also 
called CRC-CITT

### Information Field (0 to 2048 bits)

The Information Field contains the data specific to the usage of the AX.25 Transfer Frame. 
The maximum size of the Information Field is 2048 bits.

## SPINO usage 

Spino allows two usages :
- Hamradio experimentation 
- Telecomand/telemetry management for satellite

#### Hamradio experimentation 
 
 A dedicated callsign is defined for Hamradio experimentation. 
 If the associated capability of the spino is not active all packet with the correponding SSID are droped 
 
 | SSID | Usage                 | Comments                                                           | Mode | link to the description |
|------|-----------------------|---------------------------------------------------------------------|------|-------------------------|
| 0    | Not used              |                                                                     |      |                         |
| 1    | Telemetry             | used for Spino telemetry                                            |  ALL    |                         |
| 2    | Short message mailbox | Used for Short message mailbox capability                           | Mailbox     |                         |
| 3    | Digipeater            | received a message and broadcast message                            | Digipeater     |                         |
| 4    | Payload usage         | allows data exchange with cubesat OBC                                | Payload     |                         |
| 5    | Experimental          |                                                                     | Experimental     |                         |
| 6    | Not used              |                                                                     |      |                         |
| 7    |  Not used             |                                                                     |      |                         |
| 8    | Not used              |                                                                     |      |                         |
| 9    | Not used              |                                                                     |      |                         |
| 10   |  Not used             |                                                                     |      |                         |
| 11   |   Not used            |                                                                     |      |                         |
| 12   |  Not used             |                                                                     |      |                         |
| 13   |   Not used            |                                                                     |      |                         |
| 14   |   Not used            |                                                                     |      |                         |
| 15   | SPINO TM/TC           | Reserved to team in charge of Spino experiment (Command & Control)  |      |                         |



#### Telecomand/telemetry management for satellite

TBD
