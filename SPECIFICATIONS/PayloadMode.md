# Primary Payload Mode 

## Description

This mode is dedicated to the cubesat wich host the Spino card. 
It's allow 
 - to send data to  from the Cubesat OBC 
 - received data from the control station and provide data to Cubesat OBC 
 
## protocol 

### Cubesat OBC to SPINO 

	- Cubesat OBC check if dedicated register is free  (0)
	- Cubesat OBC write data in a dedicated buffer 
	- Cubesat OBC write nb bytes available in a dedicated register 
	- SPINO detect data available 
		- Spino store message in massage list 
		- Spino set dedicated register to 0 

	- Cubesat Ground Station request data 
	- SPINO send data
	
![OBC to Ground station](OBC_to_Ground_Station.png)
	
### Cubesat OBC to SPINO 

	- Cubesat Ground Station sent data to SPINO 
	- SPINO write data iin a dedicated buffer  
	- SPINO OBC write nb bytes available in a dedicated register
	- Cubesat OBC check if dedicated register as data 
	- Cubesat OBC read data from dedicated buffer 
	- Cubesat OBC write 0 in a dedicated register

![Ground station to OBC](./Ground_station_to_OBC.png)
 
## interface 

 the interface bewteen the OBC Cubesat and Spino is based on I2C communication bus
 
  
 
![I2C register](./I2C_register.png)
##

