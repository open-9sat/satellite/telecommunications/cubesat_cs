# Summary 
 
Spino embeded software manage the Spino board. According to different mode, somes functionality are activated. 

The first version is a demonstration of diffent capabilities. For your own purpose, you can adapt, add or remove capabilities.

Following chapters describe the usage of this version


## Mode 

5 modes are available :
  - [Survey Mode](./SurveyMode.md) : this mode is the default mode after Spino initialisation. In this mode, the board send telemetry and information messages if any.
  - Digipeater : this mode allows to send message to Spino wich sent back the same message. 
  - [Mailbox Mode](./MailboxMode.md) : this mode allows to store messages in Spino memmory, any user can read stored messages on request. 
  - Experimental : this mode allows to store messages from an experiment (eg Floating Buoy) in Spino memmory, any user can read stored messages on request.
  - [Payload Mode](./PayloadMode.md) : This mode allows to send and received message from the primary payload


![State diagram](./States.svg)



## Messages 

In order to comunicate with the Spino board 
- [AX25 Usage on Spino](./AX25_Usage_on_Spino.md)


- [Short message Mailbox messages](./Short_message_mailbox_messages_description.md)


