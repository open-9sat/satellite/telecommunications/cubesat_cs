| Last modification  	| Status  	|
|--------------------	|---------	|
| 21/02/2022         	| Draft   	|


#  States diagram

![State diagram](./States.svg)
*

- [Survey Mode](./SurveyMode.md)
- [Digipeater Mode] (./DigipeaterMode.md) @todo
- [Mailbox Mode](./MailboxMode.md)
- [Experimental Mode] (./ExperimentalMode.md) @todo
- [Payload Mode](./PayloadMode.md)


# Capability versus mode 

| Capabilties  	 | Survey | Digipeater | Mailbox | Experimental | Payload |  
|:---------------|:------:|:----------:|:-------:|:------------:|:-------:|
| Spino  Beacon  | X   	  |      X     |    X    |     X        |   X     |
| Command core   | X   	  |      X     |    X    |     X        |   X     |
| retransmit msg |     	  |      X     |         |              |         |
| Mailbox        |     	  |            |    X    |     X        |         |
| TLE data       |     	  |            |         |     X        |         |
| Expe Beacon    |     	  |            |         |     X        |         |
| Exchange OBC   |     	  |            |         |              |    X    |




