

| Last modification  	| Status  	|
|--------------------	|---------	|
| 05/03/2022         	| Draft   	|


# Survey mode 

## scope

When the board start or if a reset is received, the software goes in  failsafe mode. Only telemetry is sent. All other functionality of the Spino card is on idle mode or power off.

## Communication mode

- Failsafe FSK 2400bd modulation 
