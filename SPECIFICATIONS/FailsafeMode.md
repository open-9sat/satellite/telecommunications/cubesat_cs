
| Last modification  	| Status  	|
|--------------------	|---------	|
| 21/02/2022         	| Draft   	|


# Failsafe mode 

## scope

When the board start or if a reset is received, the software goes in  failsafe mode. Only telemetry is sent. All other functionality of the Spino card is on idle mode or power off.

## Communication mode

- Failsafe FSK 2400bd modulation 

After communications to the community via WEBsite and Twitter, other modulations will be tested..)


## Usage 

### system reset (Service Message)

###  setting of modulations used (Service Message)

###  activation / deactivation of a beacon mode and periodicity (Service Message)